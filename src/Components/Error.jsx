const Error = ({message}) => {
    return (
        <div className="red darken-4 error">{message}</div>
    );
}
 
export default Error;