const Weather = ({result}) => {
    // Extraer los valores
    const {name, main } = result;

    if(!name) {
        return null;
    }

    // Grados kelvin
    const kelvin = 273.15;

    return (
        <div className="card-panel white col s12">
            <div className="black-text">
                <h2>El clima de {name} es: </h2>
                <p className="temperatura">{parseFloat(main.temp - kelvin, 10).toFixed(2)} <pan>&#x2103;</pan></p>
                <p>Temperatura máxima {parseFloat(main.temp_max - kelvin, 10).toFixed(2)} <pan>&#x2103;</pan></p>
                <p>Temperatura mínima {parseFloat(main.temp_min - kelvin, 10).toFixed(2)} <pan>&#x2103;</pan></p>
            </div>
        </div>
    );
}
 
export default Weather;