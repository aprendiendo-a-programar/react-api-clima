import { useState } from 'react';
import Error from './Error';

const Form = ({search, setSearch, setConsult}) => {
    const [error, setError] = useState(false);

    // Extraer ciudad y país
    const {city, country } = search;

    // Función que coloca los elementos en el state
    const handleChange = (e) => {
        // Actualizar el state
        setSearch({
            ...search,
            [e.target.name] : e.target.value
        });
    }

    // Cuando el usuario envía el formulario
    const handleSubmit = (e) => {
        e.preventDefault();

        // Validar
        if(city.trim() === '' || country.trim() === '') {
            setError(true);
            return;
        }
        setError(false);
        setConsult(true);
    }

    return (
        <form onSubmit={handleSubmit}>

            {error ? <Error message="Ambos campos son obligatorios" /> : null}

            <div className="input-field col s12">
                <input
                    type="text"
                    name="city"
                    id='city'
                    value={city}
                    onChange={handleChange}
                />
                <label htmlFor="city">Ciudad: </label>
            </div>

            <div className="input-field col s12">
                <select
                    name="country"
                    id="country"
                    value={country}
                    onChange={handleChange}
                >
                    <option value="">-- Seleccione un país --</option>
                    <option value="ES">España</option>
                    <option value="US">Estados Unidos</option>
                    <option value="MX">México</option>
                    <option value="AR">Argentina</option>
                    <option value="CO">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="PE">Perú</option>
                </select>
                <label htmlFor="country">País: </label>
            </div>

            <div className="input-field col s12">
                <input
                    type="submit"
                    value="Buscar clima"
                    className="btn-form"
                />
            </div>
        </form>
    );
}
 
export default Form;