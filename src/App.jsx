import { useState, useEffect } from 'react';
import Header from './Components/Header';
import Form from './Components/Form';
import Weather from './Components/Weather';
import Error from './Components/Error';
import './App.css';

function App() {
  // State del form
  const [search, setSearch] = useState({
    city: '',
    country: ''
  });
  const [consult, setConsult] = useState(false);
  const [result, setResult] = useState({});
  const [error, setError] = useState(false);

  const {city, country} = search;

  useEffect(() => {
    const consultAPI = async () => {
      if(consult) {
              const appId = '14b0228c8545fcae0b481594a718d4e8';
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${appId}`;

      const answer = await fetch(url);
      const result = await answer.json();

      setResult(result);
      setConsult(false);
      }

      // Detecta si hubo resultados incorrectos
      if(result.cod === '404') {
        setError(true);
      } else {
        setError(false);
      }
    }
    consultAPI();
  }, [consult, city, country, result.cod]);

  let component;
  if(error) {
    component = <Error message="No hay resultados" />;
  } else {
    component = <Weather result={result} />
  }

  return (
    <>
      <Header
        title='Clima React App'
      />

      <div className="contenedor-form">
        <div className="row ">
          <div>
            <Form
              search={search}
              setSearch={setSearch}
              setConsult={setConsult}
            />
          </div>
            {component}
        </div>
      </div>
    </>
  );
}

export default App;
